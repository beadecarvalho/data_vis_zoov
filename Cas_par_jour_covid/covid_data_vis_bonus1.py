

import folium
import json
import pandas as pd
import numpy as np
import folium.plugins as plugins
from IPython.display import display
from datetime import datetime, timedelta

#------- Etape 1 : Je recupere les points centraux de chaque pays du monde et je les mets dans un DataFrame -------
with open('countries_centroids.json') as centroids_data:
	centroids_dict = json.load(centroids_data)

name_list =[]
lat_list = []
long_list = []

for elmt in centroids_dict:
	name_list.append(elmt['name'])
	lat_list.append(elmt['lat'])
	long_list.append(elmt['long'])

#ce dataframe est indexe par les noms de pays et comporte 2 colonnes : latitude et longitude
centroids_df = pd.DataFrame(list(zip(lat_list, long_list)), index = name_list , columns = ['Latitude','Longitude']) 



# ------- Etape 2 : Je recupere les données nombre total de cas de COVID-19 confirmés au 13/04/2021 par pays --------
covid_data = pd.read_csv('daily-new-confirmed-cases-of-covid-19.csv')

#Je filtre les informations qui m'intéressent : les noms de pays, et le nombre de cas cumulés de COVID-19 à la date du 13/04/2021
entity_list = []
cases_list = []
dates_list = []

for i in range(covid_data.shape[0]):
	entity_list.append(covid_data['Entity'][i])
	cases_list.append(covid_data['Daily new confirmed cases due to COVID-19 (rolling 7-day average, right-aligned)'][i])
	dates_list.append(datetime.strptime(covid_data['Day'][i], '%Y-%m-%d'))


covid_df = pd.DataFrame(list(zip(dates_list,cases_list)), index = entity_list, columns = ['Date','Cas par jour']) 

# -------- Etape 3 : Je fusionne les deux DataFrames en ne gardant que les pays pour lesquels on a une latitude, une longitude et un nombre de cas cumulés de COVID-19 au 13/04/2021


#J'applique les mêmes corrections que pour le script covid_data_vis.py
centroids_df.rename(index={'Brunei Darussalam':'Brunei'},inplace=True)
centroids_df.rename(index={'Cabo Verde':'Cape Verde'},inplace=True)
centroids_df.rename(index={'United States of America':'United States'},inplace=True)
centroids_df.rename(index={'Republic of Korea':'South Korea'},inplace=True)
centroids_df.rename(index={'Democratic Republic of the Congo':'Democratic Republic of Congo'},inplace=True)
centroids_df.rename(index={'Czech Republic':'Czechia'},inplace=True)
covid_df.rename(index={'North Macedonia':'Macedonia'},inplace=True)
centroids_df.rename(index={'Federated States of Micronesia':'Micronesia (country)'},inplace=True)
centroids_df.rename(index={'Republic of Moldova':'Moldova'},inplace=True)
centroids_df.rename(index={'Russian Federation':'Russia'},inplace=True)
centroids_df.rename(index={'Syrian Arab Republic':'Syria'},inplace=True)
centroids_df.rename(index={'Timor-Leste':'Timor'},inplace=True)


covid_centroids_df = centroids_df.merge(covid_df, how='inner', left_index=True, right_index=True) 

#Je retire les lignes pour lesquelles il n'y a pas de valeur de nombre de cas par jour
covid_centroids_df = covid_centroids_df[covid_centroids_df['Cas par jour'].notna()]



#------- Etape 4 : La map est générée puis sauvegardée dans le fichier HTML map_bonus1.html

#Mise en forme des données pour créer le heatMap
heat_data = []
for _,d in covid_centroids_df.groupby('Date'):
	heat_data.append([[row['Latitude'],row['Longitude'],row['Cas par jour']] for _,row in d.iterrows()])

#Creation des index a afficher sur la map
delta = max(covid_centroids_df['Date'])-min(covid_centroids_df['Date'])
time_index = [(min(covid_centroids_df['Date'])+ timedelta(days=i)).strftime('%d/%m/%Y') for i in range(delta.days+1)]

#Creation de la map
center_coordinates = (0.00, 0.00)
covid_Map = folium.Map(location=center_coordinates, tiles="stamentoner", zoom_start=2)

plugins.HeatMapWithTime(heat_data, 
	auto_play= True,
	index= time_index,
	radius= 5,
	gradient={0.1: 'blue', 0.3: 'lime', 0.6: 'orange', 0.8: 'red'},
	min_opacity=0.5, 
	max_opacity=0.8,
	scale_radius=True,
	min_speed=5.0).add_to(covid_Map)

title_html = '''
             <h3 align="center" style="font-size:20px"><b>Evolution des nombres de cas confirmés de COVID-19 par jour dans le monde</b></h3>
             '''
covid_Map.get_root().html.add_child(folium.Element(title_html))

covid_Map.save('covid_map_bonus1.html')

