

import folium
import json
import pandas as pd
import numpy as np
from IPython.display import display

#------- Etape 1 : Je recupere les points centraux de chaque pays du monde et je les mets dans un DataFrame -------
with open('countries_centroids.json') as centroids_data:
	centroids_dict = json.load(centroids_data)

name_list =[]
lat_list = []
long_list = []

for elmt in centroids_dict:
	name_list.append(elmt['name'])
	lat_list.append(elmt['lat'])
	long_list.append(elmt['long'])

#ce dataframe est indexe par les noms de pays et comporte 2 colonnes : latitude et longitude
centroids_df = pd.DataFrame(list(zip(lat_list, long_list)), index = name_list , columns = ['Latitude','Longitude']) 
#print(np.shape(centroids_df)) # => 193 pays


# ------- Etape 2 : Je recupere les données nombre total de cas de COVID-19 confirmés au 13/04/2021 par pays --------
covid_data = pd.read_csv('daily-new-confirmed-cases-of-covid-19.csv')

#Je filtre les informations qui m'intéressent : les noms de pays, et le nombre de cas cumulés de COVID-19 à la date du 13/04/2021
entity_list = []
cases_list = []

for i in range(covid_data.shape[0]):
	if covid_data['Day'][i]=='2021-04-13':
		entity_list.append(covid_data['Entity'][i])
		cases_list.append(covid_data['Total confirmed cases of COVID-19'][i])

covid_df = pd.DataFrame(cases_list, index = entity_list, columns = ['Cas cumules']) 
#print(np.shape(covid_df)) # => 207 pays

# -------- Etape 3 : Je fusionne les deux DataFrames en ne gardant que les pays pour lesquels on a une latitude, une longitude et un nombre de cas cumulés de COVID-19 au 13/04/2021
# Je vérifie les differences d'ecriture des noms de pays pour ne pas en exclure plus que nécessaire.
# for i in range(193):
# 	if not(name_list[i] in entity_list):
# 		print(name_list[i])

# print('----------------------------------')

# for i in range(207):
# 	if not(entity_list[i] in name_list):
# 		print(entity_list[i])

#J'applique les corrections
centroids_df.rename(index={'Brunei Darussalam':'Brunei'},inplace=True)
centroids_df.rename(index={'Cabo Verde':'Cape Verde'},inplace=True)
centroids_df.rename(index={'United States of America':'United States'},inplace=True)
centroids_df.rename(index={'Republic of Korea':'South Korea'},inplace=True)
centroids_df.rename(index={'Democratic Republic of the Congo':'Democratic Republic of Congo'},inplace=True)
centroids_df.rename(index={'Czech Republic':'Czechia'},inplace=True)
covid_df.rename(index={'North Macedonia':'Macedonia'},inplace=True)
centroids_df.rename(index={'Federated States of Micronesia':'Micronesia (country)'},inplace=True)
centroids_df.rename(index={'Republic of Moldova':'Moldova'},inplace=True)
centroids_df.rename(index={'Russian Federation':'Russia'},inplace=True)
centroids_df.rename(index={'Syrian Arab Republic':'Syria'},inplace=True)
centroids_df.rename(index={'Timor-Leste':'Timor'},inplace=True)


covid_centroids_df = centroids_df.merge(covid_df, how='inner', left_index=True, right_index=True) 
# print(np.shape(covid_centroids_df)) # => 184 pays restants


#------- Etape 4 : La map est générée et sauvegardée dans le fichier HTML map.html

center_coordinates = (0.00, 0.00)
covid_Map = folium.Map(location=center_coordinates, zoom_start=2)

for lat, lon, nb_cases, pays in zip(covid_centroids_df['Latitude'],covid_centroids_df['Longitude'], covid_centroids_df['Cas cumules'],covid_centroids_df.index):
	folium.CircleMarker([lat, lon], 
		radius = 0.000005*int(nb_cases), 
		popup = (pays+'<br>''Total cases: '+ str(nb_cases) ),
		color ='b',
		fill_color = 'orange',
        fill = True,
        fill_opacity = 0.7).add_to(covid_Map)

title_html = '''
             <h3 align="center" style="font-size:20px"><b>Nombre de cas cumulés de COVID-19 par pays au 13/04/2021</b></h3>
             '''
covid_Map.get_root().html.add_child(folium.Element(title_html))

covid_Map.save('covid_map.html')



