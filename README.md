# Data_vis_zoov

## Content

This project is devided in 2 parts (2 different folders):
- Cas_cumules_covid
- Cas_par_jour_covid

## Cas_cumules_covid 

Contains the Python script and the 2 datasets necessary to generate a map presenting a static visualisation of the number of cumulated COVID-19 cases in the world until April 13th 2021.

**Script execution :**  

To execute the script, you need to install Python and the necessary packages. Executing the script will generate the static map of COVID-19 cumulated cases in the world in a html file (covid_map.html). In order to see the map just open the html file.

To execute the script enter the following command line: **python covid_data_vis.py**

The map presents pop-ups containing the name of the country and the actual number of cumulated cases in the country.

## Cas_par_jour_covid (Bonus)

Contains the Python script and the 2 datasets necessary to generate a map presenting a dynamic visualisation of the evolution of the number of dayly cases (on a week average) of COVID-19 cases in the world from April 25th 2020 to April 13th 2021.

**Script execution :**  

To execute the script, you need to install Python and the necessary packages. Executing the script will generate the dynamic map of the evolution of COVID-19 dayly cases in the world in a html file (covid_map_bonus1.html). In order to see the map just open the html file.

To execute the script enter the following command line: **python covid_data_vis_bonus1.py**

By clcking once on the map, a time bar appears on the bottom left in order to see the date corresponding to what we see and to control the progress of the visualization.

## Data sources

Data for the geographic coodinates of the center of the countries:
file:///Users/beatrizdecarvalho/Downloads/415eb3175249fff3714b14c1c51582cc.html

Data on dayly and cumuated COVID-19 cases since January 2020:
https://ourworldindata.org/coronavirus

